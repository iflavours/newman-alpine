Newman on Alpine Linux
======================

This repository contains an Alpine Dockerized [Newman](https://github.com/postmanlabs/newman), published to the public Docker Hub via automated build mechanism.

This image is based on [mhart/alpine-node](https://hub.docker.com/r/mhart/alpine-node/) repository.

Rationale
---------
This image was designed to be used in a [Gitlab pipeline](http://docs.gitlab.com/ce/ci/pipelines.html), so instead
of adding Newman in a Gitlab runner, the step in charge of running the integration tests uses a tiny (that's why Alpine) docker image
with Newman pre-installed in it.

Usage
-----

### General usage

Please notice this image is configured with a workdir `/scripts`, so make sure you mount such volume first.

**Scenario**: you have exported your Postman collection as json file `some.collection.json`, and environment varialbes into `some.environment.json`.

**Objective**: run the collection against the specified environment, so we can check whether some test has failed.

```bash
docker run -ti -v $(pwd):/scripts --rm test-newman:latest \
  newman run some.collection.json \
  -e some.environment.json \
  -r cli,junit \
  --reporter-junit-no-assertions \
  --reporter-junit-export \
  --reporter-cli-no-assertions \
  report.xml

┌─────────────────────────┬──────────┬──────────┐
│                         │ executed │   failed │
├─────────────────────────┼──────────┼──────────┤
│              iterations │        1 │        0 │
├─────────────────────────┼──────────┼──────────┤
│                requests │       20 │        0 │
├─────────────────────────┼──────────┼──────────┤
│            test-scripts │       20 │        0 │
├─────────────────────────┼──────────┼──────────┤
│      prerequest-scripts │       17 │        0 │
├─────────────────────────┼──────────┼──────────┤
│              assertions │      117 │        0 │
├─────────────────────────┴──────────┴──────────┤
│ total run duration: 2s                        │
├───────────────────────────────────────────────┤
│ total data received: 17.38KB (approx)         │
├───────────────────────────────────────────────┤
│ average response time: 17ms                   │
└───────────────────────────────────────────────┘
```

Above script will write a file named as `report.xml`, which we can use to count the number of `AssertionFailure` items and take some further actions:

```bash
test $(grep -o -c '\<AssertionFailure\>' report.xml) -eq 0 && echo "Great!" || echo "Ooooops!!!"
```


Configuration
-------------
This docker image is based on the following stack:
- OS: Alpine Linux
- OpenJDK Runtime 8
- Newman: latest=3.4.4 (see [tag list](https://registry.hub.docker.com/u/iflavoursbv/newman-alpine/tags/) for more versions)


Dependencies
------------
- none

History
-------
* 0.4.0 - Update Newman to 3.4.4 and use NodeJS 8
* 0.3.0 - Update Newman to 3.4.3
* 0.2.0 - Remove the entry point and add OpenJDK Runtime 8
* 0.1.0 - Initial version

License
-------

[Licensed under MIT License](https://bitbucket.org/iflavours/newman-alpine/raw/master/LICENSE)

